### About ###

JavaScript application that given a number of pennies will calculate the minimum number of Sterling coins needed to make that amount. Eg. 123p = 1 x £1, 1 x 20p, 1 x 2p, 1 x 1p

### Usage ###

Download/clone the repository, and open index.html in a browser. Then enter a Sterling amount (e.g. £12.34) and press Enter.

### Unit Testing ###
Unit tests for the Javascript files using the Jasmine testing framework.
Open file in testing/RunTesting.html