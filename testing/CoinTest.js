describe("Convert Currency Value", function() {

  it("convertVal() should convert '100' to '£1'", function() {
    expect(View.convertVal('100')).toEqual('£1');
  });

  it("convertVal() should convert '200' to '£2'", function() {
    expect(View.convertVal('200')).toEqual('£2');
  });

  it("convertVal() should convert '50' to '50p'", function() {
    expect(View.convertVal('50')).toEqual('50p');
  });

  it("convertVal() should convert '1' to '1p'", function() {
    expect(View.convertVal('1')).toEqual('1p');
  });

});

describe("Input Validation", function() {
  var response;

  beforeEach(function () {
    response = {
      'status': false,
      'message': ''
    };
  });

  describe("Empty string", function() {
    it("to fail with error 'Input contains no characters.'", function() {
      response.message = 'Input contains no characters.';
      expect(Validation.checkInput('')).toEqual(response);
    });
  });

  describe("'0'", function() {
    it("to fail with error 'Input must be greater than 0.'", function() {
      response.message = 'Input must be greater than 0.';
      expect(Validation.checkInput('0')).toEqual(response);
    });
  });

  describe("'£p'", function() {
    it("to fail with error 'Input contains no numbers.'", function() {
      response.message = 'Input contains no numbers.';
      expect(Validation.checkInput('£p')).toEqual(response);
    });
  });

  describe("Non-Numerical Characters", function() {

    describe("'1x'", function() {
      it("to fail with error 'Input contains unaccepted non-numerical characters.'", function() {
        response.message = 'Input contains unaccepted non-numerical characters.';
        expect(Validation.checkInput('1x')).toEqual(response);
      });
    });

    describe("'£1x.0p'", function() {
      it("to fail with error 'Input contains unaccepted non-numerical characters.'", function() {
        response.message = 'Input contains unaccepted non-numerical characters.';
        expect(Validation.checkInput('£1x.0p')).toEqual(response);
      });
    });

  });

});
