$(function() {
  // Input proccess
  $('#coin-input').bind('keypress', function(e) {
    if (e.which === 13) {
      e.preventDefault();
      View.hideElements();

      // Users input validation
      var response = Validation.checkInput(this.value)
      if (response.status === true) {
        var amount = Parsing.new(this.value);
        var results = Counting.minCoins(amount);
        View.displayResult(results);
      } else {
        View.displayError(response.message);
      }
    }
  });
});

/**
 * Displaying data in the UI
 */
var View = {

  hideElements: function() {
    $('#error-alert').hide();
    $('#results-box').hide();
  },

  displayError: function(msg) {
    $('#error-alert').html('<p>Error: ' + msg + '</p>')
    $('#error-alert').show();
  },

  displayResult: function(res) {
    var html = this.toHTML(res);
    $('#results').html(html);
    $('#results-box').show();
  },

  toHTML: function(hash) {
    var html = '<ul>'
    for (var val in hash) {
      html += '<li>' + hash[val] + ' x <span class="badge badge-warning">' + this.convertVal(val) + '</span></li>';
    }
    html += '</ul>'
    return html;
  },

  convertVal: function(val) {
    if (val === '100' || val === '200') {
      val = '£' + val[0];
    } else {
      val += 'p';
    }
    return val;
  }
};

/**
 * Validate user input from
 */
var Validation = {

  checkInput: function(str) {
    str = this.removeSpace(str);
    var response = {
      'status': false,
      'message': ''
    };

    if (!str) {
      response.message = 'Input contains no characters.';
    } else if (str === '0'){
      response.message = 'Input must be greater than 0.';
    } else if (this.containsAlpha(str)) {
      response.message = 'Input contains unaccepted non-numerical characters.';
    } else if (this.containsNoNumeric(str)) {
      response.message = 'Input contains no numbers.';
    } else {
      response.status = true;
    }

    return response;
  },

  removeSpace: function(str) {
    return str.replace(/\s+/g, '');
  },

  containsAlpha: function(str) {
    var regex = /[^£.p\d]/g;
    return regex.test(str);
  },

  containsNoNumeric: function(str) {
    var regex = /\d+/g;
    return !regex.test(str);
  }
};

/**
 * This functionality to calculate minimum number of coins needed to make given amount.
 */
var Counting = {
  // Denominations to be used
  currency: [200, 100, 50, 20, 10, 5, 2, 1],

  minCoins: function(p) {
    var results = {},
        currCoin;
    var x = 0;
    while (p) {
      currCoin = this.currency[x++];
      if (p >= currCoin) {
        results[currCoin] = this.numberOfCoins(p, currCoin);
        p = this.remaining(p, currCoin);
      }
    }
    return results;
  },

  numberOfCoins: function(pennies, coin) {
    return Math.floor(pennies / coin);
  },

  remaining: function(pennies, coin) {
    return pennies % coin;
  }
};

/**
 * Object holding functionality to parse user input from String into Float.
 */
var Parsing = {

  new: function(str) { var convert = false;
    if (this.isPound(str)) {
      convert = true;
    }
    str = this.removeNonNumeric(str);

    var num = parseFloat(str);
    if (this.isDecimal(num) || convert) {
      num = this.convertToPennies(num);
    }

    return num;
  },

  isPound: function(str) {
    return (str.indexOf('£') !== -1);
  },

  removeNonNumeric: function(str) {
    return str.replace(/[£p]+/g, '');
  },

  isDecimal: function(num) {
    return (num % 1 !== 0);
  },

  convertToPennies: function(num) {
    return (num.toFixed(2) * 100);
  }
};
